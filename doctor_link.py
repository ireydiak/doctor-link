import urllib.request
import json
import sys
from http.client import RemoteDisconnected

# Global variable for representing the stream we want to write to
# I made it a global variable to avoid having to reopen the file at every request
# Instead I create a stream once and then close it once the process is terminated
EXAMINATION_FILE = ""
FILENAME = "broken_links"


class Colors:
    SUCCESS = "\033[92m"
    FAIL = "\033[91m"
    END = "\033[0m"
    WARNING = "\033[93m"


def pprint_end(warnings: int, errors: int, total: int):
    """
    Prints the end result of the examination
    :param warnings:
    :param errors:
    :param total:
    :return void
    """
    if errors + warnings == 0:
        print(Colors.SUCCESS + "{}/{} passed".format((total - warnings - errors), total) + Colors.END)
    else:
        print("{}/{} passed".format((total - warnings - errors), total))
        print(Colors.FAIL + "{} failed".format(errors) + Colors.END)
        print(Colors.WARNING + "{} warning(s)".format(warnings) + Colors.END)
        print("Results can be found in {}".format(FILENAME))


def find_broken_links(base_url_string: str, links: list) -> (int, int):
    """
    Core of the examination: attempt to connect to the remote host through a GET HTTP request
    :param base_url_string: str
    :param links: list
    :return: int, int
    """
    ierrors = 0
    iwarnings = 0
    for link in links:
        try:
            with urllib.request.urlopen(base_url_string + link) as response:
                print(".", end="", flush=True)
                response.close()

        except urllib.request.HTTPError:
            ierrors += 1
            EXAMINATION_FILE.write(link + "\n")
        except RemoteDisconnected:
            # A host might detect this script as a DOS attack, in which case the user needs to manually verify the
            # address
            iwarnings += 1
            EXAMINATION_FILE.write(link + "\n")

    print("\n")

    return ierrors, iwarnings


def load_json_from_file(filename: str) -> list:
    """
    Load a file and convert its data to JSON
    :param filename: str
    :return str
    """
    with open(filename) as f:
        d = json.load(f)

    return d


def read_options() -> (str, str):
    """
    Read the command line options given by the user
    :return: str, str
    """
    try:
        base_url_str = sys.argv[2]
    except IndexError:
        base_url_str = ""

    return sys.argv[1], base_url_str


if __name__ == '__main__':
    try:
        file, base_url = read_options()
        json_data = load_json_from_file(file)
        EXAMINATION_FILE = open("./" + FILENAME, mode="w", encoding="UTF-8")
        num_errors, num_warnings = find_broken_links(base_url, json_data)

        pprint_end(num_warnings, num_errors, len(json_data))
        EXAMINATION_FILE.close()
    except IndexError:
        print("Requires at least one argument: (1) path to json file, (2 - optional) base url")
    except FileNotFoundError:
        print("File was not found")
    except json.decoder.JSONDecodeError:
        print("Invalid JSON: the expected input is a valid JSON array of strings.")
    except PermissionError:
        print("Doctor Link cannot write to the current directory.")
